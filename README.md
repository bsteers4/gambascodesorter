<center>
<h1><ul>A Gambas3 code sorting tool</ul></h1>
<h3> Written by Bruce Steers </h3>
<p>
<hr>
<p>
</center>

Use this tool to sort your gambas3 source code functions.

Load a gambas3 .class or .module file and

Sort alphabetically and/or seperate into groups via GUI gadgets.

<pre>
Options:
 -m --modal             No file load box
 -n --nosource          No source file view
 -o --output            Save button outputs text not save file
 -c --close             Save button exits after save.
 -f --file \<Path\>       File to load
 -V --version           Display version
 -h --help              Display this help
  </pre>
 
* -m , will not show the Load file option, used if launching gui with a file.
* -n , hide the source file view
* -o , Sorted file is not saved but output to stdout.
* -c , gui auto closes on saving.
* -f , a file to load (the -f is not required)

<img src="./snapshot.png">

